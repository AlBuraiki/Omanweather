var express = require('express');
var app = express();
var bodyParser = require('body-parser');
//var queryString = require( "querystring" );
 var request = require('request');

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

//connect to the database
const mysql = require('mysql')

const connection = mysql.createConnection({ host: 'localhost', port: '3306', user: 'root', password: '', database: 'omanweather' })

//POST: insert a new city API
app.post('/insertCity', function (req, res) {
   console.log( req.body);
   //get POST data
        
    //create the sql insert statement
    var query = 'INSERT INTO cities (apiKey, name) VALUES ("' + req.body.apiKey + '", "' + req.body.name + '")';
    //execute sql statement
    connection.query(query, (err, rows) => {
        if (err) sendResponse( Error(`database error: ${err}`) );
        return sendResponse(null, rows)
    });
    
     var sendResponse = function (err, data)
    {
        if(err)
            res.status(404).end();
        else{
            res.send("data added successfully")
            console.log("data added:")
            res.status(200);
        }  
    } 
});


//POST: insert a new user API
app.post('/insertUser', function (req, res) {
   console.log( req.body);
   //get POST data
        
    //create the sql insert statement
    var query = 'INSERT INTO users (name, email, phone, isPaid, dateRegistered) VALUES ("' + req.body.name + '", "' + req.body.email + '", "' + req.body.phone + '", ' + req.body.isPaid + ', "' + req.body.dateRegistered + '")';
    //execute sql statement
    connection.query(query, (err, rows) => {
        if (err) sendResponse( Error(`database error: ${err}`) );
        return sendResponse(null, rows)
    });
    
     var sendResponse = function (err, data)
    {
        if(err)
            res.status(404).end();
        else
            {
                res.status(200);
                res.send("user added sucessfully")
                
                console.log("user added")
            }   
    }
    
});

//POST: save favourite city for user
app.post('/insertFavouriteCity', function (req, res) {
   console.log( req.body);
   //get POST data
        
    //create the sql insert statement
    var query = 'INSERT INTO favouritecities (userId, cityId) VALUES (' + req.body.userId + ', ' + req.body.cityId + ')';
    //execute sql statement
    connection.query(query, (err, rows) => {
        if (err) sendResponse( Error(`database error: ${err}`) );
        return sendResponse(null, rows)
    });
    
     var sendResponse = function (err, data)
    {
        if(err)
            res.status(404).end();
        else
            {
                 res.status(200); 
                res.send("data added sucessfully")
            }   
    }
    
});


//GET: full weather report for a specific city
app.get('/weather/:city', function (req, res){
    
    var apiKey;
    var data = [];
    
    //get the api key for the city from the database
    connection.query('SELECT apiKey FROM cities WHERE name="' + req.params.city + '"',function(err,rows){
          
        if(err) throw err;
        apiKey = rows[0].apiKey;
        //res.send("city apiKey is " + rows[0].apiKey);
        //get weather data for this city
        if(apiKey){
            console.log("apikey is:" + apiKey)
            request('http://api.openweathermap.org/data/2.5/weather?id='+apiKey +'&APPID=a230e061fac3260e36d920db667c127f', function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    
                    var obj1 = JSON.parse(body)
                    
                    data.push(obj1);
                    //if we would like to compare the city to another
                     if(req.query.compare){
                
                        connection.query('SELECT apiKey FROM cities WHERE name="' + req.query.compare + '"',function(err,rows){
                              
                            if(err) throw err;
                            if(rows.length < 1) return sendResponse(data)
                            
                            apiKey2 = rows[0].apiKey;
                            request('http://api.openweathermap.org/data/2.5/weather?id='+apiKey2 +'&APPID=a230e061fac3260e36d920db667c127f', function (error, response, body) {

                                var obj2 = JSON.parse(body)
                                data.push(obj2);
                                
                                sendResponse(data);
                            })
                        })
                       
                     }
                    else{
                        sendResponse(data)
                    }
  
                }
                else{
                    console.log("some error:" + error)
                    res.status(404).end()
                }
            }) 
        }
        else{
            res.send("City was not found")
        }
    });
   
    var sendResponse = function(data){
        res.status(200)
        res.send(JSON.stringify(data))
        
    }
   
})

//GET: only request field from weather report


//GET a specific user favourite cities
app.get('/getFavs/:userId', function (req, res) {
    connection.query('SELECT name FROM cities INNER JOIN favouritecities ON cities.cityId =  favouritecities.cityId WHERE favouritecities.userId=' + req.params.userId,function(err,rows){
        
        var data = JSON.stringify(rows)
         
        res.status(200)
        res.send(data)
        
    })
})


//GET: list of all available city
app.get('/getallcities', function (req, res) {
    connection.query('SELECT name FROM cities',function(err,rows){
        
        var data = JSON.stringify(rows)
        
        res.status(200)
        res.send(data)
       
        
    })
})

//POST: update a user API
app.post('/updateUser/:userId', function (req, res) {
  
   //get POST data
        
    //create the sql insert statement
    var query = 'UPDATE users SET  name="' + req.body.name + '", email="' + req.body.email + '", phone="' +  req.body.phone + '", isPaid=' + req.body.isPaid + ', dateRegistered="' + req.body.dateRegistered + '" WHERE userId=' +  req.params.userId ;
    //execute sql statement
    connection.query(query, (err, rows) => {
        if (err) sendResponse( Error(`database error: ${err}`) );
        return sendResponse(null, rows)
    });
    
     var sendResponse = function (err, data)
    {
        if(err)
            res.status(404).end();
        else
            {
                res.status(200)
                res.send("user updated sucessfully")
                
                console.log("user updated")
            }   
    }
    
});

//delete city
app.delete('/deletecity/:cityId', function (req, res) {

    connection.query('DELETE FROM cities WHERE cityId=' + req.params.cityId,function(err,rows){
        
        res.status(200)
        res.send("city has been deleted")
       
    })
  
})

//delete favcity
app.delete('/deleteFavcity/:userId/:cityId', function (req, res) {

    connection.query('DELETE FROM favouritecities WHERE cityId=' + req.params.cityId + ' AND userId=' + req.params.userId ,function(err,rows){
        
        res.status(200)
        res.send("favourite city has been deleted")
       
    })
  
})

//delete user
app.delete('/deleteUser/:userId', function (req, res) {

    connection.query('DELETE FROM users WHERE userId=' + req.params.userId,function(err,rows){
        
        res.status(200)
        res.send("user has been deleted")
       
    })
  
})


//start the server
var server = app.listen(8081, function () {

   var host = server.address().address
   var port = server.address().port

   console.log("Weather app listening at http://%s:%s", host, port)
});

//{"name":"Sara", "email":"sara@test.com", "phone":"87458", "isPaid":"true", "dateRegistered": "2016-12-08 15:45:00"}
//openweather data email: lorosa@zain.site password:openweather 